﻿## INSTALACION Y CREACION DE LA BASE DE DATOS

sudo apt-get update
sudo apt-get install python-pip python-dev libpq-dev postgresql postgresql-contrib nginx

sudo -u postgres psql

postgres =# CREATE DATABASE DASHBOARD;
postgres =# CREATE USER spaceag WITH PASSWORD 'thinkbig';
postgres =# ALTER ROLE spaceag SET client_encoding TO 'utf8';
postgres =# ALTER ROLE spaceag SET default_transaction_isolation TO 'read committed';
postgres =# ALTER ROLE spaceag SET timezone TO 'UTC';
postgres =# GRANT ALL PRIVILEGES ON DATABASE DASHBOARD TO spaceag;
postgres =# ALTER ROLE spaceag SUPERUSER;
postgres =# \q

## CREAR UNA AMBIENTE VIRTUAL EN PYTHON
sudo -H pip install --upgrade pip
sudo -H pip install virtualenv
mkdir ~/dashboard 
cd ~/dashboard
virtualenv dashboardenv 
source dashboardenv/bin/activate

## INSTALACION DJANGO EN EL AMBIENTE Y CONFIGURACION DEL PROJECTO DASHBOARD
pip install django gunicorn psycopg2
django-admin.py startproject dashboard ~/dashboard
nano ~/dashboard/dashboard/settings.py ### para ca,biar la configuracion

#CAMBIAR LO SIGUENTE EN SETTING.PY

ALLOWED_HOSTS = ['example.com','0.0.0.0']

INSTALLED_APPS = [
'django.contrib.admin',
'django.contrib.auth',
'django.contrib.contenttypes',
'django.contrib.sessions',
'django.contrib.messages',
'django.contrib.staticfiles',
'django.contrib.gis',
]

#EN DATABASES SE ESCOGE django.contrib.gis.db.backends.postgis YA QUE ES UN POSTGRES PERO CON CAPACIDAD DE ASIMILAR INFORMACION GEOGRAFICA

DATABASES = {
'default': {
'ENGINE': 'django.contrib.gis.db.backends.postgis',
    'NAME': 'dashboard',
'USER': 'spaceag',
'PASSWORD': 'thinkbig',
'HOST': 'localhost',
    'PORT': '',
}
}

STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

## SE HACEN LAS PRIMERAS MIGRACIONES AL PROYECTO DASHBOARD

~/dashboard/manage.py makemigrations
~/dashboard/manage.py migrate

## SE CREA EL SUPER USUARIO DEL PROJECTO DASHBOARD Y SE PERMITE EL PUERTO 8000
~/dashboard/manage.py createsuperuser
sudo ufw allow 8000 

## SE CREA LA APP ANALYTIC
python manage.py startapp analytic

# EDITO MODEL.PY CON

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

from datetime import datetime
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from public_id.fields import PublicIdField

class Organization(models.Model):
name = models.CharField(max_length=200, unique=True)
key = models.CharField(max_length=200)
admins = models.ManyToManyField(User)
project = models.ManyToManyField('Project')

class Project(models.Model):
name = models.CharField(max_length=200)
location = models.CharField(max_length=200)

class Cartography(models.Model):
name = models.CharField(max_length=200)
unit_name = models.CharField(max_length=20)
project = models.ForeignKey('Project')

class CartographicUnit(models.Model):
name = models.CharField(max_length=200)
unit_name = models.CharField(max_length=20)
polygon = models.PolygonField(srid=4326,null=True)
cartography = models.ManyToManyField('Cartography')

class CUState(models.Model):
unit = models.OneToOneField('CartographicUnit')
crop = models.CharField(max_length=20)
variety = models.CharField(max_length=20)
sowing_date = models.DateTimeField()
pruning_date = models.DateTimeField()
harvest_date = models.DateTimeField()
flowering_date = models.DateTimeField()
laimax_date = models.DateTimeField()
fructification_date = models.DateTimeField()
madurity_date = models.DateTimeField()

class Variable(models.Model):
name = models.CharField(max_length=50)
owner = models.OneToOneField('Project')
categorie = models.CharField(max_length=50)
requires_time = models.DateTimeField()
####falta la parte de type

class VariableCartography(models.Model):
project = models.OneToOneField('Project')
variables = models.OneToOneField('Variable')
cartography =models.OneToOneField('Cartography')
unit_scale = models.CharField(max_length=20)

class Filter(models.Model):
owner = models.ManyToManyField(User)
project = models.ManyToManyField('Project')
id_public = PublicIdField(auto=True)

# EDITO ADMIN.PY PAR AGRERGAR LOS MODELOS DE FORMA AUTOMATICA

# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

# Register your models here.
from django.contrib.admin import * 
from django.apps import apps

from django.contrib.admin.sites import AlreadyRegistered

app_models = apps.get_app_config('analytic').get_models()
for model in app_models:
try:
admin.site.register(model)
except AlreadyRegistered:
pass

#EDITO SETTING.PY DEL PROYECTO DASHBOARD OTRA VEZ AUMENTANDO 'ANALYTIC'

INSTALLED_APPS = [
'django.contrib.admin',
'django.contrib.auth',
'django.contrib.contenttypes',
'django.contrib.sessions',
'django.contrib.messages',
'django.contrib.staticfiles',
'django.contrib.gis',
'analytic',
]

#APLICO LAS MIGRACIONES OTRA VEZ Y LO VEO EN EL DASHBOARD
~/dashboard/manage.py makemigrations
~/dashboard/manage.py migrate
~/dashboard/manage.py runserver 0.0.0.0:8000

#PERO TENGO UN ERROR SI ES QUE DESEO AGRAGAR UN NUEVO MODELO COMO 'CUNotes' A LA APP 'ANALYTIC'

class CUNotes(models.Model):
    cu = models.OneToOneField('CartographicUnit')
note = models.CharField(max_length=1000)
datetime = models.DateTimeField(datetime.now(), blank=True)
response = models.CharField(max_length=1000)

# para ello corro los siguentes codigos OTRA VEZ:

~/dashboard/manage.py makemigrations
~/dashboard/manage.py migrate

# Sin embargo cuando se observa en el browser el error es: 'not exit relation analityc.CUNotes'. Asimismo si destruyo los la carpeta migration donde se aloja todo lo que se va a observar en el browser y vuelvo a correr

~/dashboard/manage.py makemigrations
~/dashboard/manage.py migrate

# El error persiste...


