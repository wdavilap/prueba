# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
# Register your models here.

from django.contrib.admin import *  # PART 1

from django.apps import apps

from django.contrib.admin.sites import AlreadyRegistered

app_models = apps.get_app_config('analytic').get_models()
for model in app_models:
    try:
        admin.site.register(model)
    except AlreadyRegistered:
        pass
