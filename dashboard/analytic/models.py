# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

from datetime import datetime
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from public_id.fields import PublicIdField

class Organization(models.Model):
    name = models.CharField(max_length=200, unique=True)
    key = models.CharField(max_length=200)
    admins = models.ManyToManyField(User)
    project = models.ManyToManyField('Project')

class Project(models.Model):
    name = models.CharField(max_length=200)
    location = models.CharField(max_length=200)

class Cartography(models.Model):
    name = models.CharField(max_length=200)
    unit_name = models.CharField(max_length=20)
    project = models.ForeignKey('Project')

class CartographicUnit(models.Model):
    name = models.CharField(max_length=200)
    unit_name = models.CharField(max_length=20)
    polygon =  models.PolygonField(srid=4326,null=True)
    cartography =  models.ManyToManyField('Cartography')

class CUState(models.Model):
    unit = models.OneToOneField('CartographicUnit')
    crop = models.CharField(max_length=20)
    variety =  models.CharField(max_length=20)
    sowing_date = models.DateTimeField()
    pruning_date = models.DateTimeField()
    harvest_date = models.DateTimeField()
    flowering_date = models.DateTimeField()
    laimax_date = models.DateTimeField()
    fructification_date = models.DateTimeField()
    madurity_date = models.DateTimeField()

class Variable(models.Model):
    name = models.CharField(max_length=50)
    owner = models.OneToOneField('Project')
    categorie = models.CharField(max_length=50)
    requires_time = models.DateTimeField()
####falta la parte de type

class VariableCartography(models.Model):
    project = models.OneToOneField('Project')
    variables = models.OneToOneField('Variable')
    cartography =models.OneToOneField('Cartography')
    unit_scale = models.CharField(max_length=20)

class Filter(models.Model):
    owner = models.ManyToManyField(User)
    project = models.ManyToManyField('Project')
    id_public = PublicIdField(auto=True)

# class CUNotes(models.Model):
#     cu = models.OneToOneField('CartographicUnit')
#     note = models.CharField(max_length=1000)
#     datetime = models.DateTimeField(datetime.now(), blank=True)
#     from =
#     attachment =
#     to =
#     response = models.CharField(max_length=1000)
# Create your models here.
